gsap.timeline({
  scrollTrigger: {
    trigger: ".animation",
    start: "bottom bottom",
    end: "top, top",
    scrub: true
  }
})
.to(".scroll", {yPercent: 15}, 0)
.to(".animation", {yPercent: 0}, 0)